# Bot response analysis

Analyze bot response for UIUC Alexa Social Bot team, helps them find insights from users' rate of bot response

# Result 
Result of analysis can be seen in data_analysis.pdf

# Description

User_prompt: User message sent to the bot. <br>
Dialog_act_intent_dialogactintent: The categorization of the user message. <br>
Dialog_act_intent_topic: The topic of conversation categorized. <br>
Session_id: Can be considered as a unique conversation id. Can have multiple turn ids under 1 session id. <br>
Turn_id: id to indicate each step in a conversation. Unique. <br>
Search_bot_api_output: A module/ response generator output for that turn. <br>
Ranker_input: Input provided to a ranking algorithm which picks 1 out of the multiple possible responses generated for the turn. <br>
Bot_response: Final response sent back to the user. <br>
Candidate_responses: Different possible responses generated for the single user input. <br>
Ratings: Rating for the conversation on how well it performed according to the user. <br>
Conversation_duration: Total time the user spent interacting with the bot in seconds. <br>
Rest of the keys are self explanatory. <br>
